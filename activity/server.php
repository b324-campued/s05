<?php
// This line starts a new or resumes an existing session, which is essential for session-based data storage.
session_start();


// Initialize the users array with the specified user
if (!isset($_SESSION['users'])) {
    $_SESSION['users'] = array(
        (object) array('email' => 'johnsmith@gmail.com', 'password' => '1234')
    );
}

class User {
	// Class methods code
	// Login of user
	public function login($email, $password){

			/* New user is created as an object using the (object) casting. This user object has two properties: 'email' which is set to the provided $email and 'password' which is set to the provided $password*/
			$newUser = (object)[
				'email' => $email,
				'password' => $password
			];
	}

	// Logout of User
	public function logout(){
		session_destroy();

	}	
}

$user = new User();

if ($_POST['action'] === 'login') {
    $email = $_POST['email'];
    $password = $_POST['password'];
    
    foreach ($_SESSION['users'] as $user) {
        if ($user->email === $email && $user->password === $password) {
            // Successful login
            $_SESSION['loggedInUser'] = $user;
            header('Location: ./main.php');
            exit();
        }
    }
    
    // Invalid credentials
    header('Location: ./index.php');
    exit();

} else if($_POST['action'] === 'logout'){

	header('Location: ./index.php');
} 

?>