<!DOCTYPE html>
<html>
	<head>
		<title>S05: Login Form</title>
	</head>

	<body>
		<!-- This function initializes a new or existing session, allowing you to store and retrieve data across multiple requests for a particular user. -->

		<?php session_start(); ?>

		<!-- Form where users can login. The form sends a POST request to the server.php script. -->

		<form method="POST" action='./server.php'>
			<input type="hidden" name="action" value="login"/>
			Email: <input type="text" name="email" required/>
			Password: <input type="password" name="password" required/>

			<button type="submit">Login</button>
		</form>
		
	</body>
</html>
