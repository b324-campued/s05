<!DOCTYPE html>
<html>
	<head>
		<title>S05: Main</title>
	</head>

	<body>
		<!-- This function initializes a new or existing session, allowing you to store and retrieve data across multiple requests for a particular user. -->

		<?php session_start(); ?>

		 <?php if (isset($_SESSION['loggedInUser'])) { ?>

		 	<p>Hello, <?php echo $_SESSION['loggedInUser']->email; ?></p>
			<?php foreach($_SESSION['users'] as $index => $user){ ;?>


	    	<form method="POST" action='./server.php'>
            	<input type="hidden" name="action" value="logout"/>
            	<button type="submit">Logout</button>
        	</form>
			
			<?php }; ?>
		<?php }; ?>

	</body>
</html>
